<?php

/**
 * Created by PhpStorm.
 * User: ammarkalim
 * Date: 2015-10-11
 * Time: 2:37 PM
 */
class ShipLoader
{
    /**
     * @return Ship[]
     * Returns an array of ship objects
     */
    public function getShips()
    {
        $shipsData = $this->queryForShips();

        $ships = array();
        foreach($shipsData as $shipData) {
            $ships[] = $this->createShipFromData($shipData);
        }

        return $ships;

//        return ('<pre>' . var_export($ships, true) . '</pre>');

    }

    public function findOnebyId($id)
    {
        $pdo = new PDO('mysql:host=localhost;dbname=oo_battle', 'root', 'root');
        $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        $statement = $pdo->prepare('SELECT * FROM ship WHERE id = :id');
        $statement->execute(array('id' => $id));
        $shipArray = $statement->fetch(PDO::FETCH_ASSOC);

        if(!$shipArray)
        {
            return null;
        }

        return $this->createShipFromData($shipArray);

    }

    public function createShipFromData(array $shipData)
    {
        $ship = new Ship($shipData['name']);
        $ship->setId($shipData['id']);
        $ship->setWeaponPower($shipData['weapon_power']);
        $ship->setJediFactor($shipData['jedi_factor']);
        $ship->setStrength($shipData['strength']);

        return $ship;
    }

    public function queryForShips()
    {
        $pdo = new PDO('mysql:host=localhost;dbname=oo_battle', 'root', 'root');
        $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        $statement = $pdo->prepare('SELECT * FROM ship');
        $statement->execute();
        $shipsArray = $statement->fetchAll(PDO::FETCH_ASSOC);

        return $shipsArray;
    }



}