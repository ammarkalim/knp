<?php

/**
 * Created by PhpStorm.
 * User: ammarkalim
 * Date: 2015-10-11
 * Time: 4:12 PM
 */
class BattleResult
{
    private $usedJediPower;

    private $winningShip;

    private $losingShip;

    public function __construct($usedJediPower, Ship $winningShip = null, Ship $losingShip = null)
    {
        $this->usedJediPower = $usedJediPower;
        $this->winningShip = $winningShip;
        $this->losingShip = $losingShip;
    }

    /**
     * @return boolean
     */
    public function wereJediPowerUsed()
    {
        return $this->usedJediPower;
    }

    /**
     * @return Ship|null
     */
    public function getWinningShip()
    {
        return $this->winningShip;
    }

    /**
     * @return Ship|null
     */
    public function getLosingShip()
    {
        return $this->losingShip;
    }

    /**
     * @return bool
     */
    public function isThereAWinnder()
    {
        return $this->getWinningShip() != null;
    }
}